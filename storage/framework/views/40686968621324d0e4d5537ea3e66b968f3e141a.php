<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo e(config('app.name', 'Laravel')); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(asset('css/ionicons.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('css/AdminLTE.min.css')); ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo e(asset('css/blue.css')); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b><?php echo e(config('app.name', 'Laravel')); ?></b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-box-msg"> 
      <?php if($errors->has('email')): ?>
        <span class="invalid-feedback alert text-red" role="alert">
          <strong><?php echo e($errors->first('email')); ?></strong>
        </span>
      <?php endif; ?>
    </div>

    <?php if(session('status')): ?>
      <div class="alert alert-success">
        <span class="glyphicon glyphicon-envelope"></span> &nbsp;
        <?php echo e(session('status')); ?>

      </div>
    <?php endif; ?>
    <?php if(session('warning')): ?>
      <div class="alert alert-warning">
          <span class="glyphicon glyphicon-warning-sign"></span> &nbsp;
        <?php echo e(session('warning')); ?>

      </div>
    <?php endif; ?>
    
    <form action="<?php echo e(route('login')); ?>" method="post">
      <?php echo csrf_field(); ?>
      <div class="form-group has-feedback<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
        <input id="email" type="email" name="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        
      </div>
      <div class="form-group has-feedback<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
        <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

        <?php if($errors->has('password')): ?>
          <span class="invalid-feedback text-red" role="alert">
            <strong><?php echo e($errors->first('password')); ?></strong>
          </span>
        <?php endif; ?>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    

   <?php if(Route::has('password.request')): ?>
      <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
        <?php echo e(__('Forget Password ?')); ?>

      </a>
    <?php endif; ?>
    <a href="<?php echo e(route('register')); ?>" class="text-center">Registration</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<!-- iCheck -->
<script src="<?php echo e(asset('js/icheck.min.js')); ?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
