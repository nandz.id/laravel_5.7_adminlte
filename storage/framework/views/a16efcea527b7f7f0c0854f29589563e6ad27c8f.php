<?php $__env->startSection('content'); ?>
<div class="">
  <div class="">
       
    <section class="content-header">
      <h1>
       Laravel 5.7
        <small>keep learn</small>
      </h1>
      

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Dashboard</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        
          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3><?php echo e($count_karyawan); ?></h3>
                <p>Karyawan</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
            </div>
          </div>
           
        
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        &nbsp;
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.llamada', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /var/www/html/laravel_adminlte/resources/views/home.blade.php */ ?>