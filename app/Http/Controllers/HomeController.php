<?php

namespace App\Http\Controllers;
// namespace App\Models;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Jabatan;
use DB;
use Notification;
use App\Notifications\MyFirstNotification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count_karyawan = Karyawan::count();
        return view('home', compact('count_karyawan'));
    }

    public function sendNotification() {

        $user = Karyawan::first();

        $details = [
            'greeting'   => 'hello World',
            'body'       => 'This is my notification from nandz.id@gmail.com',
            'thanks'     => 'Thanks you bla bla bla',
            'actionText' => 'View My Site',
            'actionURL'  => url('/'),
            'order_id'   => 101
        ];

        Notification::send($user, new MyFirstNotification($details));
        dd('done');
        // Notification::route('mail', Auth::user()->email)->notify;
    }
}
