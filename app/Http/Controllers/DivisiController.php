<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Divisi;
use Excel;
use App\Exports\DivisiReport;

class DivisiController extends Controller
{
    public function __construct() {

        $this->class_error = "has-error";
    }

    public function index() {

        $divisi = Divisi::all();

        // return view('divisi.index', compact('divisi'));

        //api
        return response()->json(['status' => 'success',
                                 'data'   => $divisi]);

    }

    public function create() {

        $class_error = $this->class_error;
        return view('divisi.create', compact('class_error'));
    }

    public function store (Request $request) {

        $this->validate($request, [
            'nama_divisi' => 'required|string|max: 50',
            'keterangan'  => 'required|string|max: 255'
        ],
        [
            'nama_divisi.required' => 'Nama Divisi wajib diisi !',
            'keterangan.required'  => 'Keterangan wajib diisi !'
        ]);

        // $divisi = new Divisi;
        // $divisi->nama_divisi = $request->nama_divisi;
        // $divisi->keterangan  = $request->keterangan;
        // $divisi->save();
        // Alert::success('Data Divisi berhasil ditambahkan !','Success');
            
        // return redirect('divisi')->with('Success','Data Divisi berhasil ditambahkan !');

        if(Divisi::create($request->all())) {
            return response()->json(['status'  => 'success', 'message' => 'Data has been created'],201);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Internal Server Error'],500);
        }

    }

    public function show($id) {
        $divisi = Divisi::find($id);
        if ($divisi) {
          return response()->json(['status' => 'success', 'data'=> $divisi]);
        }
 
        return response()->json(['status' => 'error', 'message' => 'Data not found'],404);
    }

    public function edit($id) {

        $divisi = Divisi::find($id);
        $class_error = $this->class_error;
        return view('divisi.edit', compact('divisi','class_error', 'id'));

    }

    public function update (Request $request, $id) {

        $this->validate($request, [
            'nama_divisi' => 'required|string|max: 50',
            'keterangan'  => 'required|string|max: 255'
        ],
        [
            'nama_divisi.required' => 'Nama Divisi wajib diisi !',
            'keterangan.required'  => 'Keterangan wajib diisi !'
        ]);

        $divisi = Divisi::find($id);
        // $divisi->nama_divisi = $request->nama_divisi;
        // $divisi->keterangan  = $request->keterangan;
        // $divisi->save();
        // Alert::success('Data Divisi berhasil diubah !','Success');
        

        // return redirect('divisi')->with('Success', 'Data Divisi berhasil diubah !');
        if($divisi) {
            $divisi->update($request->all());
            return response()->json(['status' => 'success', 'message' => 'Data has been update']);
        } 

        return response()->json(['status' => 'error', 'message' => 'Cannot updating data'], 400);

    }

    public function destroy ($id) {
        $divisi = Divisi::find($id);
        // $divisi->delete();
        // Alert::error('Data Divisi berhasil dihapus !','Success');

        // return redirect('divisi')->with('Success', 'Data berhasil di delete');
        if($divisi) {
            $divisi->delete();
            return response()->json(['status' => 'success', 'message' => 'Data has been deleted']);
        }

        return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }

    public function laporanExcel()
    {
        return Excel::download(new DivisiReport, 'divisi.xlsx');
    }





}
