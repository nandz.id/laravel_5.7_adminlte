<?php

namespace App\DataTables;

use App\Models\Jabatan;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    // public function dataTable($query)
    // {
    //     return datatables($query)
    //         ->addColumn('action', 'userdatatable.action');
    // }

    // public function ajax()
    // {
    //     return $this->datatables
    //         ->eloquent($this->query())
    //         ->make(true);
    // }



    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */

    // public function query(User $model)
    // {
    //     return $model->newQuery()->select('id', 'add-your-columns-here', 'created_at', 'updated_at');
    // }

    public function query()
    {
        $users = Jabatan::select();

        return $this->applyScopes($users);
    }

    

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {

       return $this->builder()
                    ->columns($this->getColumns())
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'buttons' => ['export', 'print', 'reset', 'reload'],
                    ]);
        


        // return $this->builder()
            // ->addIndexColumn()
            // ->addColumn('action', function ($jabatan) {
            //     return '<form action='.action('JabatanController@destroy', $jabatan->id).'" method="post">
            //     <a href="'.action('JabatanController@edit', $jabatan->id).'" class="btn btn-sm btn-success" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
            //     '.Form::token().'
            //     <input name="_method" type="hidden" value="DELETE">
            //     <button class="btn btn-sm btn-danger" data-toggle="tooltip" title="Edit" type="submit"><i class="fa fa-trash"></i></button>
            //   </form>';
            // })
            // ->editColumn('id', 'ID: {{$id}}')
            // ->columns([
            //     'nama_jabatan',
            //     'divisi_id',
            //     'keterangan'
            // ])
            // ->parameters([
            //     'dom' => 'Bfrtip',
            //     'buttons' => ['csv', 'excel', 'pdf', 'print', 'reset', 'reload'],
            // ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'nama_jabatan',
            'divisi_id',
            'keterangan'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
