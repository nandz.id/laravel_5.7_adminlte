<?php
namespace App\Exports;

use App\Models\Divisi;
use Maatwebsite\Excel\Concerns\FromCollection;

class DivisiReport implements FromCollection
{
    public function collection()
    {
        return Divisi::all();
    }
}
