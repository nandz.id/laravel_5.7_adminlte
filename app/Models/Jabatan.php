<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model {
    
    protected $table = 'jabatan';
    protected $fillable = [
        'nama_jabatan',
        'divisi_id',
        'keterangan'
    ];

    public function karyawan() {
        
        return $this->hasOne('App\Models\Karyawan');
    }

    public function divisi() {
        
        return $this->belongsTo('App\Models\Divisi');
    }


    
}
