<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    protected $table = 'divisi';
    protected $fillable = [
        'nama_divisi',
        'keterangan'
    ];
    
    public function jabatan() {
        return $this->hasOne('App\Models\jabatan');
    }
}
