<?php

use Carbon\Carbon;

if (! function_exists('tgl_id')) {
    function tgl_id($tgl)
    {
        $dt = new Carbon($tgl);
        setlocale(LC_TIME, 'id');
        return $dt->formatLocalized('%d %B %Y');   
    }
}

if (!function_exists('rupiah'))
{
	function rupiah($nilai) 
	{
		return number_format($nilai,0,',',',');
	}
}

if (!function_exists('format_rupiah'))
{
	function format_rupiah($nilai)
	{
		return 'Rp.'.number_format($nilai,0,',','.');
	}
}

if (!function_exists('format_uang'))
{
	function format_uang($nilai) 
	{
		return number_format($nilai,0,'.',',');
	}
}

if (!function_exists('format_angka'))
{
	function format_angka($nilai)
	{
		return number_format($nilai,2,'.',',');
	}
}
