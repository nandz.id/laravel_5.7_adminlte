<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Jabatan;

class JabatanTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Jabatan $jabatan)
    {
        return [
            'nama_jabatan' => $jabatan->nama_jabatan,
            'divisi'  => [
                'id'  => $jabatan->divisi_id,
                'nama_divisi' => $jabatan->divisi->nama_divisi,
            ],
            'keterangan'   => $jabatan->keterangan,
            'created_at'   => $jabatan->created_at->diffForHumans(),
            'updated_at'   => $jabatan->updated_at->diffForHumans()
            // 'nama_divisi' => $jabatan->divisi->nama_divisi
            
        ];
    }
}
