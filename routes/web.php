<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\DataTables\UsersDataTable;

// Route::get('users', function(UsersDataTable $dataTable) {
//     return $dataTable->render('users.index');
// });

Route::get('/welcome', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::resource('karyawan','KaryawanController')->middleware('auth');
Route::resource('jabatan', 'JabatanController')->middleware('auth');
Route::resource('divisi', 'DivisiController')->middleware('auth');

Route::get('/jabatanlist','JabatanController@jabatanList');

//Excel Download
Route::get('/excel', 'DivisiController@laporanExcel');

Route::get('send', 'HomeController@sendNotification');



// Route::group(['middleware' => ['web', 'auth', 'roles']], function() {
//     Route::group(['roles' => 'user'],function() {
//         Route::resource('jabatan', 'JabatanController');
//         Route::resource('karyawan', 'JabatanController');

//     });
//     Route::group(['roles' => 'admin',],function() {
//         Route::resource('divisi', 'DivisiController');

//     });

// });

