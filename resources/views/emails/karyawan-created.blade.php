@component('mail::message')
# Introduction

Selamat Bergabung Mr/Mrs. {{ $karyawan->nama_lengkap }}.
Data anda telah ditambahkan ke Master Karyawan.

@component('mail::button', ['url' => url('/karyawan/' . $karyawan->id)])
View Karyawan
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
