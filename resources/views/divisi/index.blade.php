@extends('layouts.llamada')
@section('content')

<script type="text/javascript">
  $(document).ready(function(){	
    $('#example').dataTable({
      "bPaginate": true,
      "bInfo": true,	
      "bSort": true
    }); 
  $('#alert_success').fadeTo(2000, 500).slideUp(500, function(){ $("#success-alert").slideUp(500);});   
  });	
</script>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Jabatan</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
      
      <div class="box-body">   
        @if (\Session::has('success'))
          <div class="alert alert-success alert-dismissible" id="alert_success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="fa fa-check"></i> {{ \Session::get('success') }} !</h5>
          </div>
        @endif
        <div class="form-group">
          <a href="{{ url('divisi/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
        </div>

        <table id="example" class="table table-bordered table-striped table-condensed">
        <thead>
          <tr>
            <th width="5%">No. </th>
            <th width="35%">Nama Divisi</th>
            <th width="35%">Keterangan</th>
            <th width="15%">Action</th>
          </tr>
        </thead>
        <tbody>
          @php ($no = 1)
          @foreach($divisi as $val)
            <tr>
              <td>{{$no}}</td>
              <td>{{$val->nama_divisi}}</td>
              <td>{{$val->keterangan}}</td>
              
              <td align="center">
                <form action="{{action('DivisiController@destroy', $val->id)}}" method="post">
                  <a href="{{action('DivisiController@edit', $val->id)}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-sm btn-danger" data-toggle="tooltip" title="Edit" type="submit"><i class="fa fa-trash"></i></button>
                </form>
                {{-- <form action="{{action('KaryawanController@destroy', $val->id)}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <a class="btn btn-sm btn-danger" type="submit" id="submit">Delete</a>
                </form> --}}
              </td>
            </tr> 
            @php ($no++)
          @endforeach
        </tbody>
      </table>
      {{-- {{ $karyawan->onEachSide(1)->links() }} --}}
      </div>
    </div>
@endsection