@extends('layouts.llamada')
@section('content')
<script type="text/javascript">
  $(document).ready(function() {

    $('.dateinput').datepicker({
      format: "yyyy/mm/dd",
      autoclose: true,
      // endDate: "+0d",
      yearRange: "yy-50:yy+1",
    });

    $('#nik').prop('readonly',true);

});
</script>
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Karyawan Tambah Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="post" action="{{url('karyawan')}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif
          <div class="form-group {{ $errors->has('nik') ? $class_error : "" }}">
            <label>NIK </label>
          <input name="nik" class="form-control" id="nik" value="{{ old('nik') ?? $nik }}" />
          <small class="text-danger">{{ $errors->first('nik') }}</small>
          </div>
          <div class="form-group {{ $errors->has('nama_lengkap') ? $class_error : "" }}">
            <label>Nama Karyawan </label>
          <input name="nama_lengkap" class="form-control" id="nama_lengkap" value="{{ old('nama_lengkap') }}" />
          <small class="text-danger">{{ $errors->first('nama_lengkap') }}</small>
          </div>
          <div class="form-group {{ $errors->has('tempat_lahir') ? $class_error : "" }}">
            <label>Tempat Lahir </label>
          <input name="tempat_lahir" class="form-control" id="tempat_lahir" value="{{ old('tempat_lahir') }}" />
          <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
          </div>
          <div class="form-group {{ $errors->has('tanggal_lahir') ? $class_error : "" }}">
            <label>Date:</label>

            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input name="tanggal_lahir" type="text" class="form-control pull-right dateinput" id="tanggal_lahir" value="{{ old('tanggal_lahir') }}">
            </div>
            <!-- /.input group -->
            <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
          </div>
          <div class="form-group {{ $errors->has('jk') ? $class_error : "" }}">
            <label>Jenis Kelamin </label>
            <select class="form-control" name="jk" >
              <option value="">--Pilih Jenis Kelamin--</option>
              <option value="L" @if(old('jk')=="L") selected @endif>Laki-laki</option>
              <option value="P" @if(old('jk')=="P") selected @endif>Perempuan</option>
            </select>
            <small class="text-danger">{{ $errors->first('jk') }}</small>
          </div>
          <div class="form-group {{ $errors->has('jabatan_id') ? $class_error : "" }}">
            <label>Jabatan</label>
            <select class="form-control" name="jabatan_id">
              <option value="">--Pilih Jabatan--</option>
              @foreach ($jabatan as $val)
                <option value="{{ $val->id }}" @if(old('jabatan_id')==$val['id']) selected @endif> {{ $val->nama_jabatan }} </option>
              @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('jabatan') }}</small>
          </div>
          <div class="form-group {{ $errors->has('email') ? $class_error : "" }}">
            <label>Email </label>
          <input name="email" class="form-control" id="email" value="{{ old('email') }}" />
          <small class="text-danger">{{ $errors->first('email') }}</small>
          </div>
          <div class="form-group {{ $errors->has('alamat') ? $class_error : "" }}">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control" id="alamat" rows="6">{{ old('alamat') }}</textarea>
            <small class="text-danger">{{ $errors->first('alamat') }}</small>
          </div>
          <div class="form-group {{ $errors->has('foto') ? $class_error : "" }}">
            <label>Foto </label>
          <input type="file" name="foto" class="form-control" id="foto" value="{{ old('foto') }}" />
          <small class="text-danger">{{ $errors->first('foto') }}</small>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
