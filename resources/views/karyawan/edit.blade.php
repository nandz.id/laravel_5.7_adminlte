@extends('layouts.llamada')
@section('content')
<script type="text/javascript">
  $(document).ready(function() {

    $('.tanggal_input').datepicker({
      format: "yyyy/mm/dd",
      autoclose: true,
      // endDate: "+0d",
      yearRange: "yy-50:yy+1",
    });

    $('#nik').prop('readonly',true);

});
</script>
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Karyawan Edit Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="POST" action="{{action('KaryawanController@update', $id)}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif
          <input name="_method" type="hidden" value="PATCH">
          <div class="form-group {{ $errors->has('nik') ? $class_error : "" }}">
            <label>NIK </label>
          <input name="nik" class="form-control" id="nik" value="{{ $karyawan['nik'] }}" />
          <small class="text-danger">{{ $errors->first('nik') }}</small>
          </div>
          <div class="form-group {{ $errors->has('nama_lengkap') ? $class_error : "" }}">
            <label>Nama Karyawan </label>
          <input name="nama_lengkap" class="form-control" id="nama_lengkap" value="{{ $errors->has('nama_lengkap') ? "" : $karyawan['nama_lengkap'] }}">
          <small class="text-danger">{{ $errors->first('nama_lengkap') }}</small>
          </div>
          <div class="form-group {{ $errors->has('tempat_lahir') ? $class_error : "" }}">
            <label>Tempat Lahir </label>
          <input name="tempat_lahir" class="form-control" id="tempat_lahir" value="{{ $errors->has('tempat_lahir') ? "" : $karyawan['tempat_lahir'] }}">
          <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
          </div>
          <div class="form-group {{ $errors->has('tanggal_lahir') ? $class_error : "" }}">
            <label>Tanggal Lahir </label>
          <input type="text" name="tanggal_lahir" class="form-control tanggal_input" id="tanggal_lahir" value="{{ $errors->has('tanggal_lahir') ? "" : $karyawan['tanggal_lahir'] }}">
          <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
          </div>
          <div class="form-group {{ $errors->has('jk') ? $class_error : "" }}">
            <label>Jenis Kelamin </label>
            <select class="form-control" name="jk">
              <option value="L" @if($karyawan['jk']=="L") selected @endif>Laki-laki</option>
              <option value="P" @if($karyawan['jk']=="P") selected @endif>Perempuan</option>
            </select>
            <small class="text-danger">{{ $errors->first('jenis_kelamin') }}</small>
          </div>
          <div class="form-group {{ $errors->has('email') ? $class_error : "" }}">
            <label>Email </label>
          <input name="email" class="form-control" id="email" value="{{ $errors->has('email') ? "" : $karyawan['email'] }}">
          <small class="text-danger">{{ $errors->first('email') }}</small>
          </div>
          <div class="form-group {{ $errors->has('jabatan_id') ? $class_error : "" }}">
            <label>Jabatan</label>
            <select class="form-control" name="jabatan_id">
              @foreach ($jabatan as $val)
                <option value="{{ $val['id'] }}" @if($karyawan['jabatan_id']==$val['id']) selected @endif>{{$val['nama_jabatan']}}</option>
              @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('jabatan') }}</small>
          </div>
          <div class="form-group {{ $errors->has('alamat') ? $class_error : "" }}">
            <label>Alamat </label>
            <textarea name="alamat" class="form-control" id="alamat" rows="6">{{ $errors->has('alamat') ? "" : $karyawan['alamat'] }}</textarea>
            <small class="text-danger">{{ $errors->first('alamat') }}</small>
          </div>
          <div class="form-group {{ $errors->has('foto') ? $class_error : "" }}">
            <label>Foto </label>
            <div class="clearfix" ></div>
            @if ($karyawan['foto'] == "")
              <img src="{{ url('img/default-image.png')}}" width="300px" height="300px">
            @else
              <img src="{{ Storage::url('img/500/'.$karyawan['foto'])}}" width="300px" height="300px">
            @endif
            <input type="file" name="foto" class="form-control" id="foto" value="" />
            <small class="text-danger">{{ $errors->first('foto') }}</small>
          </div>

        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
