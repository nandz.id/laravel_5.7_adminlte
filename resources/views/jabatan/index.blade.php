@extends('layouts.llamada')
@section('content')

<script type="text/javascript">
  $(document).ready(function(){	
    $('#example').dataTable({
      "bPaginate": true,
      "bInfo": true,	
      "bSort": true
    }); 
  $('#alert_success').fadeTo(2000, 500).slideUp(500, function(){ $("#success-alert").slideUp(500);});   

  $('#hapus').click(function() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
  })
 
  });	
</script>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Jabatan</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
      
      <div class="box-body">   
        @if (\Session::has('success'))
          <div class="alert alert-success alert-dismissible" id="alert_success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="fa fa-check"></i> {{ \Session::get('success') }} !</h5>
          </div>
        @endif
        <div class="form-group">
          <a href="{{ url('jabatan/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
        </div>

        <table id="jabatan_list" class="table table-bordered table-striped table-condensed text-centered">
          <thead>
            <tr>
              <th width="5%">No. </th>
              <th width="25%">Nama Jabatan</th>
              <th width="25%">Nama Divisi</th>
              <th width="35%">Keterangan</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
      </table>
      <script type="text/javascript">
        $(document).ready(function() {
          var oTable = $('#jabatan_list').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: "{{ url('jabatanlist') }}",
            columns:
            [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'nama_jabatan', name: 'nama_jabatan'},
              {data: 'nama_divisi', name: 'nama_divisi'},
              {data: 'keterangan', name: 'keterangan'},
              {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
          });
        });
        </script>
      {{-- {{ $karyawan->onEachSide(1)->links() }} --}}
      </div>
    </div>
@endsection
